import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/presentation/presentation.dart';
import 'package:choose_picture_challenge/src/resources/resources.dart';
import 'package:choose_picture_challenge/src/utils/image_cache.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ChallengeCombineViewModel extends BaseViewModel with SocketListener {
  SocketService service;
  CombineGamePage combineGamePage;
  int questIndex;
  QuestApplication questApplication;

  final roomSubject = BehaviorSubject<Room>();
  final combineScreenController = BehaviorSubject<CombineGamePage>();
  final questController = BehaviorSubject<int>();
  final scoreControllerPlayer1 = BehaviorSubject<int>();
  final scoreControllerPlayer2 = BehaviorSubject<int>();

  final quest1Controller = BehaviorSubject<int>();

  Room _room;
  Room get room => _room;

  init(BuildContext context) async {
    service = await SocketService.instance();
    service.setListener(this);
    service.listenEvent(SocketService.socket_combine);
    onConnect();
  }

  onConnect() async {
    if (await service.isConnected) {
      combineGamePage = CombineGamePage.waitingPlayer;
      combineScreenController.sink.add(combineGamePage);
      service.emitCombineFindPlayer(2);
      questIndex = 0;
      questController.sink.add(questIndex);
      scoreControllerPlayer1.sink.add(0);
      scoreControllerPlayer2.sink.add(0);
    }
  }

  changeCombinePage(CombineGamePage _combineGamePage) {
    if (_combineGamePage != combineGamePage) {
      combineGamePage = _combineGamePage;
      combineScreenController.sink.add(_combineGamePage);
    }
  }

  void onCombineFindPlayer(data) async {
    combineGamePage = CombineGamePage.waitingPlayer;
    combineScreenController.sink.add(combineGamePage);
    _room = Room.fromJson(await data);
    roomSubject.sink.add(_room);
    Future.delayed(const Duration(seconds: 2), () {
      combineGamePage = CombineGamePage.playGame;
      combineScreenController.sink.add(combineGamePage);
    });
  }

  void onCombinePlayQuestion(data) async {
    if (await data != null) {
      questApplication = QuestApplication.fromJson(await data["data"]);
      await _preLoadData();
      Future.delayed(const Duration(milliseconds: 500), () => service.emitCombinePlayReady());
    }
  }

  void onCombinePlayShowQuestion(data) {
    if (data != null) {
      questIndex = data["questionIndex"];
      questController.sink.add(questIndex);
    }
  }

  void onCombinePlayAnswer(data) {
    _onAnswerResult(questIndex, data['answer']);
    _onUpdateScorePlayer(data);
  }

  void onCombineEmotion(data) {}

  void onCombinePlayEndGame(data) {
    combineGamePage = CombineGamePage.endGame;
    combineScreenController.sink.add(combineGamePage);
  }

  void onCombineOutRoom(data) {}

  String convertImageLink(String link) {
    String url = link.replaceAll("public/", "storage/");
    return AppEndPoint.BASE_IMAGE + "/" + url;
  }

  _preLoadData() {
    for (int i = 0; i < questApplication.quests.length; i++) {
      ImageCacheUtils.cacheImageNetwork(
          url: convertImageLink(questApplication.quests[i].image),
          context: context);
      for (int j = 0; j < questApplication.quests[i].other.length; j++) {
        ImageCacheUtils.cacheImageNetwork(
            url: convertImageLink(questApplication.quests[i].other[j].image),
            context: context);
      }
    }
  }

  _onAnswerResult(int index, dynamic data){
    switch(index){
      case 0:
        quest1Controller.sink.add(data);
        break;
      default:
        break;
    }
  }

  _onUpdateScorePlayer(dynamic data) {
    if (data["uid"] == room.player1.userInfo.uid) {
      scoreControllerPlayer1.sink.add(data["score"]);
    } else if (data["uid"] == room.player2.userInfo.uid) {
      scoreControllerPlayer2.sink.add(data["score"]);
    }
  }

  // _onPlayerAnswer(int index){
  //   switch(index){
  //     case 1:
  //       break;
  //     default:
  //       break;
  //   }
  // }
}
