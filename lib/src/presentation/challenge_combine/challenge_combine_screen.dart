import 'package:choose_picture_challenge/src/presentation/base/base.dart';
import 'package:choose_picture_challenge/src/presentation/challenge_combine/challenge_combine.dart';
import 'package:choose_picture_challenge/src/presentation/challenge_waiting_player/challenge_waiting_player.dart';
import 'package:choose_picture_challenge/src/presentation/presentation.dart';
import 'package:choose_picture_challenge/src/resources/resources.dart';
import 'package:flutter/material.dart';

enum CombineGamePage {
  waitingPlayer,
  playGame,
  endGame,
}

class ChallengeCombineScreen extends StatefulWidget {
  @override
  _ChallengeCombineScreenState createState() => _ChallengeCombineScreenState();
}

class _ChallengeCombineScreenState extends State<ChallengeCombineScreen> {
  ChallengeCombineViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ChallengeCombineViewModel>(
      viewModel: ChallengeCombineViewModel(),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel;
        _viewModel.init(context);
      },
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(
            child: StreamBuilder(
              stream: _viewModel.combineScreenController,
              builder: (context, snapshot) => _buildCombineGamePage(snapshot.data),
            ),
          ),
        );
      },
    );
  }

  Widget _buildCombineGamePage(CombineGamePage combineGamePage) {
    switch (combineGamePage) {

      case CombineGamePage.waitingPlayer:
        return StreamBuilder<Room>(
          stream: _viewModel.roomSubject.stream,
          builder: (context, snapshot) => ChallengeWaitingPlayerScreen(
            room: snapshot.data,
          ),
        );

      case CombineGamePage.playGame:
        return WidgetCombineChallengeGame(viewModel: _viewModel,);

      case CombineGamePage.endGame:
        return Center(child: Text("END GAME"),);

      default:
        return SizedBox();
    }
  }
}
