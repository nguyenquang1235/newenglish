import 'package:cached_network_image/cached_network_image.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:flutter/material.dart';
import 'package:choose_picture_challenge/src/configs/constant/constant.dart';
import 'package:rxdart/rxdart.dart';

typedef void ParamFunction();

enum StatusGameImageChoose {
  waiting,
  pass,
  wrong,
}

class WidgetGameImageChoose extends StatefulWidget {
  final String url;
  final ParamFunction paramFunction;
  final int correctAnswerId;
  final int thisId;
  final int showCorrectId;
  final bool nextQuestion;
  const WidgetGameImageChoose(
      {Key key,
      this.url,
      this.paramFunction,
      this.correctAnswerId,
      this.thisId,
      this.showCorrectId,
      this.nextQuestion})
      : super(key: key);

  @override
  _WidgetGameImageChooseState createState() => _WidgetGameImageChooseState();
}

class _WidgetGameImageChooseState extends State<WidgetGameImageChoose> {
  final _statusController = BehaviorSubject<StatusGameImageChoose>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _changeStatus(StatusGameImageChoose.waiting);
  }

  @override
  Widget build(BuildContext context) {
    _showCorrect();
    return Stack(
      children: [
        GestureDetector(
          onTap: () => widget.paramFunction(),
          child: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey,
                      blurRadius: 1,
                      spreadRadius: 1,
                      offset: Offset.zero)
                ],
                border: Border.all(color: Colors.white, width: 4),
                color: Colors.white),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: Image(
                image: CachedNetworkImageProvider(
                  AppEndPoint.BASE_IMAGE + "/" + widget.url,
                ),
              ),
            ),
          ),
        ),
        Opacity(
          opacity: 0.9,
          child: StreamBuilder(
            stream: _statusController.stream,
            builder: (context, snapshot) {
              return _buildWidgetTrueFalse(snapshot.data);
            },
          ),
        )
      ],
    );
  }

  _buildWidgetTrueFalse(StatusGameImageChoose statusGameImageChoose) {
    switch (statusGameImageChoose) {
      case StatusGameImageChoose.waiting:
        return Container();
      case StatusGameImageChoose.pass:
        return _buildWidgetTrue();
      case StatusGameImageChoose.wrong:
        return _buildWidgetFalse();
      default:
        return Container();
    }
  }

  _buildWidgetTrue() {
    return Container(
      padding: const EdgeInsets.all(4),
      child: Center(
        child: Image.asset(
          AppImages.icNewTrue,
          width: 100,
          height: 100,
        ),
      ),
    );
  }

  _buildWidgetFalse() {
    return Container(
      padding: const EdgeInsets.all(4),
      child: Center(
        child: Image.asset(
          AppImages.icNewWrong,
          width: 100,
          height: 100,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _statusController.close();
    super.dispose();
  }

  _showCorrect() {
    if (widget.nextQuestion) {
      _changeStatus(StatusGameImageChoose.waiting);
    }
    if (widget.showCorrectId != null) {
      if (widget.showCorrectId == widget.thisId) {
        if (widget.correctAnswerId == widget.thisId) {
          _changeStatus(StatusGameImageChoose.pass);
        } else {
          _changeStatus(StatusGameImageChoose.wrong);
        }
      }
    }
  }

  _changeStatus(StatusGameImageChoose statusGameImageChoose) {
    _statusController.sink.add(statusGameImageChoose);
  }
}
