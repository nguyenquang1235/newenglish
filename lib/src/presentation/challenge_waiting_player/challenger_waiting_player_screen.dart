import 'package:choose_picture_challenge/src/configs/configs.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/resources/resources.dart';
import 'package:flutter/material.dart';

class ChallengeWaitingPlayerScreen extends StatefulWidget {
  final Room room;

  const ChallengeWaitingPlayerScreen({Key key, this.room}) : super(key: key);
  @override
  _ChallengeWaitingPlayerScreenState createState() =>
      _ChallengeWaitingPlayerScreenState();
}

class _ChallengeWaitingPlayerScreenState
    extends State<ChallengeWaitingPlayerScreen> {

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: Image.asset(AppImages.bgNewFindPlayer).image, fit: BoxFit.fill),
      ),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: widget.room != null ? _buildPlayerInfo() : _buildFindPlayer(),
    );
  }

  _buildCircleAvatar(String url) {
    if (url != null) {
      if (!url.contains("https"))
        url = "${AppEndPoint.BASE_SOCKET_URL}:${AppEndPoint.PORT}$url";
    }
    return Container(
      padding: const EdgeInsets.all(4),
      width: 100,
      height: 100,
      child: CircleAvatar(
        backgroundImage: url == null
            ? AssetImage(
                AppImages.icAvatarBoy,
              )
            : NetworkImage(
                url,
              ),
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.icAvatarBelow1), fit: BoxFit.fill),
          borderRadius: BorderRadius.all(Radius.circular(100))),
    );
  }

  _buildPlayerWaitingInfo({String url, String name, bool min}) {
    return Container(
      child: Column(
        mainAxisSize: min ? MainAxisSize.min : MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildCircleAvatar(url),
          Text(name ?? "?????", style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white),),
        ],
      ),
    );
  }

  _buildPlayerInfo() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildPlayerWaitingInfo(
              url: widget.room.player1.userInfo.avatar,
              name: widget.room.player1.userInfo.fullname,
              min: false),
          _buildPlayerWaitingInfo(
            url: widget.room.player2.userInfo.avatar,
            name: widget.room.player2.userInfo.fullname,
            min: false,
          )
        ],
      ),
    );
  }

  _buildFindPlayer() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildPlayerWaitingInfo(url: null, min: true, name: null),
          _buildPlayerWaitingInfo(url: null, min: true, name: null),
        ],
      ),
    );
  }

}
