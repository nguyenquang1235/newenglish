import 'package:choose_picture_challenge/src/configs/configs.dart';
import 'package:choose_picture_challenge/src/presentation/base/base.dart';
import 'package:choose_picture_challenge/src/presentation/home/home_viewmodel.dart';
import 'package:choose_picture_challenge/src/presentation/routers.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      precacheImage(Image.asset(AppImages.bgNewFindPlayer).image, context);
      precacheImage(Image.asset(AppImages.icAvatarBoy).image, context);
      precacheImage(Image.asset(AppImages.icAvatarGirl).image, context);
      precacheImage(Image.asset(AppImages.icAvatarBelow1).image, context);
      precacheImage(Image.asset(AppImages.icHeart1).image, context);
      precacheImage(Image.asset(AppImages.icHeart2).image, context);
      precacheImage(Image.asset(AppImages.icNewCardBelow).image, context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<HomeViewModel>(
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: OutlineButton(
                    child: Text("Find Pictures"),
                    color: Colors.orange,
                    onPressed: () => Navigator.pushNamed(
                        context, Routers.challenge_find_picture),
                  ),
                ),
                Center(
                  child: OutlineButton(
                    child: Text("Quick Select"),
                    color: Colors.orange,
                    onPressed: () => Navigator.pushNamed(
                        context, Routers.challenge_quick_select),
                    // onPressed: () async {
                    //   await Permission.storage.request();
                    //   var status = await Permission.storage.status;
                    //   print(status);
                    // },
                  ),
                ),
                Center(
                  child: OutlineButton(
                    child: Text("Combine"),
                    color: Colors.orange,
                    onPressed: () =>
                        Navigator.pushNamed(context, Routers.challenge_combine),
                    // onPressed: () async {
                    //   await Permission.storage.request();
                    //   var status = await Permission.storage.status;
                    //   print(status);
                    // },
                  ),
                ),
              ],
            ),
          ),
        );
      },
      viewModel: HomeViewModel(),
      onViewModelReady: (viewModel) => viewModel.init(),
    );
  }
}
