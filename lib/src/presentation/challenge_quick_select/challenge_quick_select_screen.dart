import 'package:choose_picture_challenge/src/configs/configs.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/resources/models/challenge/challenge_model.dart';
import 'package:flutter/material.dart';
import 'package:choose_picture_challenge/src/presentation/presentation.dart';

class ChallengeQuickSelectScreen extends StatefulWidget {
  @override
  ChallengeQuickSelectScreenState createState() =>
      ChallengeQuickSelectScreenState();
}

class ChallengeQuickSelectScreenState
    extends State<ChallengeQuickSelectScreen> {
  ChallengeQuickSelectViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ChallengeQuickSelectViewModel>(
      viewModel: ChallengeQuickSelectViewModel(state: this),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel;
        _viewModel.init();
      },
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(
            child: StreamBuilder<CombineGamePage>(
              stream: viewModel.gameScreenController.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return buildQuickSelectGamePage(snapshot.data);
                } else {
                  return Container();
                }
              },
            ),
          ),
        );
      },
    );
  }

  Widget buildQuickSelectGamePage(CombineGamePage combineGamePage) {
    switch (combineGamePage) {
      case CombineGamePage.waitingPlayer:
        return StreamBuilder(
          stream: _viewModel.roomSubject,
          builder: (context, snapshot) => ChallengeWaitingPlayerScreen(
            room: snapshot.data,
          ),
        );
      case CombineGamePage.playGame:
        return buildGameScreen();
      case CombineGamePage.endGame:
        return Container();
      default:
        return SizedBox();
    }
  }

  buildGameScreen() {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.bgFindImage), fit: BoxFit.fill)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _buildChallengeHeader(),
          Expanded(
            child: _buildChallengerBody(),
          ),
        ],
      ),
    );
  }

  _buildChallengeHeader() {
    return Stack(
      children: [
        Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width,
          height: 80,
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildCircleAvatar(_viewModel.room.player1.userInfo.avatar),
              StreamBuilder(
                  stream: _viewModel.scoreAndHeartControllerPlayer1.stream,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Container();
                    }
                    return _buildHeartAndScore(
                        heart: snapshot.data["heart"],
                        score: snapshot.data["score"],
                        isReversed: true);
                  }),
              Expanded(
                  child: Image.asset(
                AppImages.icTest,
                width: 60,
                height: 60,
              )),
              StreamBuilder(
                  stream: _viewModel.scoreAndHeartControllerPlayer2.stream,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Container();
                    }
                    return _buildHeartAndScore(
                        heart: snapshot.data["heart"],
                        score: snapshot.data["score"],
                        isReversed: false);
                  }),
              _buildCircleAvatar(_viewModel.room.player2.userInfo.avatar),
            ],
          ),
        ),
      ],
    );
  }

  _buildHeartAndScore({int heart, int score, bool isReversed}) {
    List<Widget> listHeart = isReversed ?? false
        ? _buildHearts(heart)
        : _buildHearts(heart).reversed.toList();
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Wrap(
            children: listHeart,
          ),
          Text(
            score.toString(),
            style: AppStyles.DEFAULT_MEDIUM_BOLD
                .copyWith(color: Colors.orangeAccent),
          )
        ],
      ),
    );
  }

  List<Widget> _buildHearts(int heart) {
    List<Widget> list = [];
    for (var i = 0; i < 3; i++) {
      list.add(i < heart
          ? Image.asset(
              AppImages.icHeart1,
              fit: BoxFit.fill,
              width: 20,
              height: 18,
            )
          : Image.asset(
              AppImages.icHeart2,
              fit: BoxFit.fill,
              width: 20,
              height: 18,
            ));
    }
    return list;
  }

  _buildChallengerBody() {
    return Container(
        padding: const EdgeInsets.only(left: 8, right: 8),
        child: StreamBuilder(
          stream: _viewModel.questController.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: [
                  Expanded(
                    child: Center(
                      child: Text(
                        snapshot.data["data"].word,
                        style: AppStyles.DEFAULT_LARGE_BOLD,
                      ),
                    ),
                    flex: 1,
                  ),
                  Expanded(
                    flex: 14,
                    child: GridView.count(
                      padding: const EdgeInsets.symmetric(vertical: 1),
                      childAspectRatio: 1.35,
                      crossAxisCount: 2,
                      crossAxisSpacing: 8,
                      mainAxisSpacing: 8,
                      children: _buildGameQuestion(
                          snapshot.data["data"], snapshot.data["index"]),
                    ),
                  )
                ],
              );
            } else {
              return Container();
            }
          },
        ));
  }

  _buildGameQuestion(Quest quest, int index) {
    List<Quest> quests = [quest];
    List<Widget> list = [];
    for (var item in quest.other) {
      quests.add(item);
    }
    quests.shuffle();

    for (var item in quests) {
      String url = item.image.replaceAll("public/", "storage/");
      list.add(
        StreamBuilder(
          stream: _viewModel.showCorrectController.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return WidgetGameImageChoose(
                nextQuestion: snapshot.data["nextQuestion"],
                correctAnswerId: quest.wid,
                thisId: item.wid,
                showCorrectId: snapshot.data["data"],
                url: url,
                paramFunction: () async =>
                    await _viewModel.onChooseAnswer(item.wid, index),
              );
            } else {
              return Container();
            }
          },
        ),
      );
    }
    return list;
  }

  _buildCircleAvatar(String url) {
    if (url != null) {
      if (!url.contains("https"))
        url = "${AppEndPoint.BASE_SOCKET_URL}:${AppEndPoint.PORT}$url";
    }
    return Container(
      width: 55,
      height: 55,
      child: url == null
          ? Image.asset(
              AppImages.icAvatarBoy,
              fit: BoxFit.fill,
            )
          : Image.network(
              url,
              fit: BoxFit.fill,
            ),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey.shade200, width: 2.5),
          borderRadius: BorderRadius.all(Radius.circular(100))),
    );
  }
}
