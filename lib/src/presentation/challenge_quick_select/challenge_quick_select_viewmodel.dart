import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/presentation/base/base.dart';
import 'package:choose_picture_challenge/src/presentation/challenge_waiting_player/challenger_waiting_player_screen.dart';
import 'package:choose_picture_challenge/src/resources/resources.dart';
import 'package:choose_picture_challenge/src/utils/image_cache.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../presentation.dart';

class ChallengeQuickSelectViewModel extends BaseViewModel with SocketListener {
  SocketService service;
  QuestApplication questApplication;
  Room room;
  CombineGamePage combineGamePage;

  final dynamic state;

  final roomSubject = BehaviorSubject<Room>();
  final gameScreenController = BehaviorSubject<CombineGamePage>();

  final questController = BehaviorSubject<Map<String, dynamic>>();
  final questNumController = BehaviorSubject<int>();
  final showCorrectController = BehaviorSubject<Map<String, dynamic>>();
  final scoreAndHeartControllerPlayer1 =
      BehaviorSubject<Map<String, dynamic>>();
  final scoreAndHeartControllerPlayer2 =
      BehaviorSubject<Map<String, dynamic>>();

  ChallengeQuickSelectViewModel({this.state});

  init() async {
    service = await SocketService.instance();
    service.setListener(this);
    service.listenEvent(SocketService.socket_choose_picture_listener);
    onConnect();
  }

  @override
  void dispose() {
    service.emitOutRoom();
    service.offEvent(SocketService.socket_choose_picture_listener);
    super.dispose();
  }

  /// --Summary--
  ///   - Hàm được gọi sau khi connect vào socket của trò chơi
  ///   - Dùng để emit event emitFindPlayer
  ///   - Sau khi emit trả về màn hình chờ người chơi

  onConnect() async {
    if (await service.isConnected) {
      _changeCombinePage(CombineGamePage.waitingPlayer);
      service.emitFindPlayer(2);
    }
  }

  void onEmotion(data) {
    // TODO: implement onEmotion
  }

  /// --Summary--
  ///   - Hàm được gọi sau khi emitFindPlayer
  ///   - Dùng để lấy thông tin phòng chơi của server trả về, lưu thông tin lại dưới biến room
  ///   - Đẩy thông tin lấy được vào trong màn hình chờ người chơi để hiển thị thông tin 2 player

  void onFindPlayer(data) async {
    room = Room.fromJson(await data);
    roomSubject.sink.add(room);
    Map<String, dynamic> originalHeartAndScore = {"heart": 3, "score": 0};
    scoreAndHeartControllerPlayer1.sink.add(originalHeartAndScore);
    scoreAndHeartControllerPlayer2.sink.add(originalHeartAndScore);
  }

  void onOutRoom(data) async {
    // TODO: implement onOutRoom
  }

  void onPlayAnswer(data) async {
    Map<String, dynamic> params = {
      "data": await data["answer"],
      "nextQuestion": false
    };

    showCorrectController.sink.add(params);
    _onChangeScoreAndHeart(await data);
  }

  void onPlayEndGame(data) async {
    // TODO: implement onPlayEndGame
    print(await data["player1"]["heart"]);
    print(await data["player1"]["score"]);
    print(await data["player2"]["heart"]);
    print(await data["player2"]["score"]);
    _resetStream();
    onConnect();
  }

  void onPlayQuestion(data) async {
    questApplication = QuestApplication.fromJson(await data);
    if (questApplication != null) {
      _preLoadData();
      _changeCombinePage(CombineGamePage.playGame);
      Future.delayed(Duration(milliseconds: 500), () => service.emitPlayReady());
    }
  }

  void onPlayReady(data) {}

  void onPlayShowQuestion(data) async {
    Map<String, dynamic> questData = {
      "data": questApplication.quests[await data["questionIndex"]],
      "index": await data["questionIndex"],
      "nextQuestion": true
    };
    questController.sink.add(questData);
    questNumController.sink.add(await data["questionIndex"] + 1);
    showCorrectController.sink.add({"nextQuestion": true});
  }

  onChooseAnswer(int word, int index) async {
    await service.emitPlayAnswer(index: index, answer: word);
  }

  _onChangeScoreAndHeart(Map<String, dynamic> data) {
    if (data["uid"] == room.player1.userInfo.uid) {
      scoreAndHeartControllerPlayer1.sink.add(data);
    } else if (data["uid"] == room.player2.userInfo.uid) {
      scoreAndHeartControllerPlayer2.sink.add(data);
    }
  }

  _resetStream() {
    roomSubject.sink.add(null);
    Map<String, dynamic> originalHeartAndScore = {"heart": 3, "score": 0};
    scoreAndHeartControllerPlayer1.sink.add(originalHeartAndScore);
    scoreAndHeartControllerPlayer2.sink.add(originalHeartAndScore);
    questController.sink.add(null);
    questNumController.sink.add(null);
  }

  _preLoadData() {
    for (int i = 0; i < questApplication.quests.length; i++) {
      ImageCacheUtils.cacheImageNetwork(
          url: AppEndPoint.BASE_IMAGE +
              "/" +
              questApplication.quests[i].image.replaceAll("public/", "storage/"),
          context: context);
      for (int j = 0; j < questApplication.quests[i].other.length; j++) {
        ImageCacheUtils.cacheImageNetwork(
            url: AppEndPoint.BASE_IMAGE +
                "/" +
                questApplication.quests[i].other[j].image
                    .replaceAll("public/", "storage/"),
            context: context);
      }
    }
  }

  _changeCombinePage(CombineGamePage _combineGamePage) {
    if (_combineGamePage != combineGamePage) {
      combineGamePage = _combineGamePage;
      gameScreenController.sink.add(_combineGamePage);
    }
  }
}
