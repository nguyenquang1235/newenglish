class AppImages {
  AppImages._();

  // Backgrounds
  static final String bgGreen = "assets/backgrounds/bg_green.jpg";
  static final String bgChallengeHeader = "assets/images/challenge_header.png";

  static final String bgGameLeft = "assets/backgrounds/more_app_left.png";
  static final String bgGameRight = "assets/backgrounds/more_app_right.png";

  static final String bgBubble = "assets/backgrounds/bg_bubble.png";
  static final String bgBubbleGround = "assets/backgrounds/bg_bubble_ground.png";
  static final String bgBubbleGroundTransparent = "assets/backgrounds/bg_bubble_ground_transparent.png";

  static final String bgPanelTime = "assets/backgrounds/panel_time.png";
  static final String bgLearn = "assets/backgrounds/bg_learn.jpg";
  static final String bgLearnLeft = "assets/backgrounds/learn_left.png";
  static final String bgLearnRight = "assets/backgrounds/learn_right.png";
  static final String bgFindPlayer = "assets/backgrounds/bg_find_player.jpg";

  static final String bgNewFindPlayer = "assets/news/bg_find_player.png";
  static final String bgFindImage = "assets/news/bg_find_image.png";
  static final String bgTopic = "assets/news/bg_topic.png";

  // Buttons
  static final String icBtnBack = "assets/buttons/btn_back_default.png";
  static final String icBtnBackClicked = "assets/buttons/btn_back_touch.png";
  static final String icBtnBackDisable = "assets/buttons/btn_back_disable.png";

  static final String icBtnBubble = "assets/buttons/btn_bubble_default.png";
  static final String icBtnBubbleClicked = "assets/buttons/btn_bubble_touch.png";

  static final String icBtnSound = "assets/buttons/btn_bubble_sound_default.png";
  static final String icBtnSoundClicked = "assets/buttons/btn_bubble_sound_touch.png";

  static final String icBtnFindFast = "assets/buttons/btn_find_fast_default.png";
  static final String icBtnFindFastClicked = "assets/buttons/btn_find_fast_touch.png";

  static final String icBtnFindWord = "assets/buttons/btn_hang_mouse_default.png";
  static final String icBtnFindWordClicked = "assets/buttons/btn_hang_mouse_touch.png";

  static final String icBtnFast = "assets/buttons/btn_learn_fast_default.png";
  static final String icBtnFastClicked = "assets/buttons/btn_learn_fast_touch.png";

  static final String icBtnSlow = "assets/buttons/btn_learn_slow_default.png";
  static final String icBtnSlowClicked = "assets/buttons/btn_learn_slow_touch.png";

  static final String icBtnLigature = "assets/buttons/btn_initial_letters_default.png";
  static final String icBtnLigatureClicked = "assets/buttons/btn_initial_letters_touch.png";

  static final String icBtnNext = "assets/buttons/btn_next_default.png";
  static final String icBtnNextDisable = "assets/buttons/btn_next_disable.png";

  static final String btnMenu = "assets/buttons/btn_left_touch.png";
  static final String btnMenuClicked = "assets/buttons/btn_left_default.png";
  static final String btnTrue = "assets/buttons/btn_play_touch.png";

  static final String icBtnPronunciation = "assets/buttons/btn_pronunciation.jpg";

  static final String btnFindWordTrue = "assets/news/btn_find_word_true.png";
  static final String btnFindWordUnSelect = "assets/news/btn_find_word_unselect.png";
  static final String btnFindWordWrong = "assets/news/btn_find_word_wrong.png";

  static final String btnQuickChooseRemove = "assets/news/btn_quick_choose_remove.png";
  static final String btnQuickChooseTrue = "assets/news/btn_quick_choose_true.png";
  static final String btnQuickChooseUnSelect = "assets/news/btn_quick_choose_unselect.png";
  static final String btnQuickChooseWrong = "assets/news/btn_quick_choose_wrong.png";


  // Icons
  static final String icFalse = "assets/icons/false.png";
  static final String icTrue = "assets/icons/true.png";
  static final String icTest = "assets/news/test.png";


  static final String icTurn = "assets/icons/ic_turn.png";
  static final String icTurnDisable = "assets/icons/ic_turn_disable.png";

  static final String icAvatar = "assets/icons/avatar_default.png";
  static final String icAvatarFind = "assets/icons/avatar_find.png";
  static final String icBgAvatar = "assets/icons/bg_avatar.png";

  static final String icBgCardAbove = "assets/icons/bg_card_above.png";
  static final String icBgCardAboveGreen = "assets/icons/bg_card_above_green.png";
  static final String icBgCardAboveRed = "assets/icons/bg_card_above_red.png";
  static final String icBgCardBelow = "assets/icons/bg_card_below.png";
  static final String icBgLearn = "assets/icons/bg_learn.png";

  static final String icBoxFind = "assets/icons/ic_box_find.png";
  static final String icCharacterDefault = "assets/icons/btn_character_default.png";

  static final String icAvatarBelow1 = "assets/news/ic_avatar_below1.png";
  static final String icAvatarBelow2 = "assets/news/ic_avatar_below2.png";

  static final String icAvatarBoy = "assets/news/ic_avatar_boy.png";
  static final String icAvatarGirl = "assets/news/ic_avatar_girl.png";

  static final String icNewCardBelow = "assets/news/ic_card_below.png";
  static final String icTopicBelow = "assets/news/ic_topic_below.png";

  static final String icHeart1 = "assets/news/ic_heart_1.png";
  static final String icHeart2 = "assets/news/ic_heart_2.png";

  static final String icVolumeOff = "assets/news/ic_volume_off";
  static final String icVolumeOn = "assets/news/ic_volume_on";

  static final String icNewTrue = "assets/news/ic_true.png";
  static final String icNewWrong = "assets/news/ic_wrong.png";

  // Game Break Ball
  static final String gbbAirBalloon = "assets/game_break_ball/air_balloon";
  static final String gbbBallonRed = "assets/game_break_ball/answer/balloon_red1.png";
  static final String gbbBallonBlue = "assets/game_break_ball/answer/balloon_blue1.png";
  static final String gbbBallonYellow = "assets/game_break_ball/answer/balloon_yellow1.png";
  static final String gbbBallonGreen = "assets/game_break_ball/answer/balloon_green1.png";
  static final String gbbBallonPink = "assets/game_break_ball/answer/balloon_pink1.png";

  // Loading
  static final String loadingProgressContent = "assets/loading/progress_content.png";
  static final String loadingProgressContentBonus = "assets/loading/progress_content_bonus.png";
  static final String loadingProgressMark = "assets/loading/progress_mask.png";

  // Atlas
  static final String atlasBalloon = "assets/atlas/skin_balloon.atlas";
}
