import 'package:flutter/material.dart';
import 'package:choose_picture_challenge/src/configs/configs.dart';

class AppStyles {
  static const String FONT_OPTIMUS = "Optimus";

  static const FONT_SIZE_VERY_SMALL = 10.0;
  static const FONT_SIZE_SMALL = 12.0;
  static const FONT_SIZE_MEDIUM = 15.0;
  static const FONT_SIZE_LARGE = 20.0;

  static const TEXT_HEIGHT = 1.2;

  static final DEFAULT_VERY_SMALL = TextStyle(fontSize: FONT_SIZE_VERY_SMALL, height: TEXT_HEIGHT, color: AppColors.black);
  static final DEFAULT_SMALL = TextStyle(fontSize: FONT_SIZE_SMALL, height: TEXT_HEIGHT, color: AppColors.black);
  static final DEFAULT_MEDIUM = TextStyle(fontSize: FONT_SIZE_MEDIUM, height: TEXT_HEIGHT, color: AppColors.black);
  static final DEFAULT_LARGE = TextStyle(fontSize: FONT_SIZE_LARGE, height: TEXT_HEIGHT, color: AppColors.black);

  static final DEFAULT_SMALL_BOLD = DEFAULT_SMALL.copyWith(fontWeight: FontWeight.bold);
  static final DEFAULT_MEDIUM_BOLD = DEFAULT_MEDIUM.copyWith(fontWeight: FontWeight.bold);
  static final DEFAULT_LARGE_BOLD = DEFAULT_LARGE.copyWith(fontWeight: FontWeight.bold);
}
