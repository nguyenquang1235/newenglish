class Quest {
  final int wid;
  final String word;
  final String spelling;
  final String part;
  final String example;
  final String image;
  final String sound;
  final String mean;
  final List<Quest> other;

  Quest({
    this.wid,
    this.word,
    this.spelling,
    this.part,
    this.example,
    this.image,
    this.sound,
    this.mean,
    this.other,
  });

  factory Quest.fromJson(Map<String, dynamic> json) {
    // List<Quest> other = [];
    // for(var item in json["other"]){
    //   other.add(Quest.fromJson(item));
    // }
    return Quest(
        wid: json["wid"],
        image: (json["image"] == "" || json["image"] == null)
            ? null
            : json["image"],
        spelling: (json["spelling"] == "" || json["spelling"] == null)
            ? null
            : json["spelling"],
        example: (json["example"] == "" || json["example"] == null)
            ? null
            : json["example"],
        part:
            (json["part"] == "" || json["part"] == null) ? null : json["part"],
        mean:
            (json["mean"] == "" || json["mean"] == null) ? null : json["mean"],
        sound: (json["sound"] == "" || json["sound"] == null)
            ? null
            : json["sound"],
        word: json["word"],
        other: (json["other"] == null || json["other"] == "")
            ? null
            : List<Quest>.from(json["other"].map((x) => Quest.fromJson(x))));
  }
}
