class UserInfo {
  UserInfo({
    this.uid,
    this.fullname,
    this.username,
    this.avatar,
    this.phone,
    this.email,
    this.createtime,
    this.updatetime,
    this.win,
    this.lose,
    this.draw,
    this.coin,
    this.score,
    this.isonline,
    this.lastonline,
    this.unlockedvip,
    this.rank,
    this.type,
  });

  String uid;
  String fullname;
  String username;
  String avatar;
  dynamic phone;
  dynamic email;
  DateTime createtime;
  DateTime updatetime;
  int win;
  int lose;
  int draw;
  int coin;
  int score;
  int isonline;
  DateTime lastonline;
  int unlockedvip;
  int rank;
  int type;

  factory UserInfo.fromJson(Map<String, dynamic> json){
    print(json["createtime"]);
    return UserInfo(
      uid: json["uid"],
      fullname: json["fullname"],
      username: json["username"],
      avatar: json["avatar"] == null ? null : json["avatar"],
      phone: json["phone"],
      email: json["email"],
      createtime: json["createtime"] == "" ? null : DateTime.parse(json["createtime"].toString()),
      updatetime: json["updatetime"] == "" ? null : DateTime.parse(json["updatetime"].toString()),
      win: json["win"],
      lose: json["lose"],
      draw: json["draw"],
      coin: json["coin"],
      score: json["score"],
      isonline: json["isonline"] == "" ? null : json["isonline"],
      lastonline: json["lastonline"] == null ? null : DateTime.parse(json["lastonline"].toString()),
      unlockedvip: json["unlockedvip"] == null ? null : json["unlockedvip"],
      rank: json["rank"] == null ? null : json["rank"],
      type: json["type"] == null ? null : json["type"],
    );
  }

  Map<String, dynamic> toJson() => {
    "uid": uid,
    "fullname": fullname,
    "username": username,
    "avatar": avatar == null ? null : avatar,
    "phone": phone,
    "email": email,
    "createtime": createtime == null ? null : createtime.toIso8601String(),
    "updatetime": updatetime == null ? null : updatetime.toIso8601String(),
    "win": win,
    "lose": lose,
    "draw": draw,
    "coin": coin,
    "score": score,
    "isonline": isonline == null ? null : isonline,
    "lastonline": lastonline == null ? null : lastonline.toIso8601String(),
    "unlockedvip": unlockedvip == null ? null : unlockedvip,
    "rank": rank == null ? null : rank,
    "type": type == null ? null : type,
  };
}
