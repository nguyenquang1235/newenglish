import 'package:choose_picture_challenge/src/resources/services/socket/socket_listener.dart';

class SocketChoosePictureListener with SocketListener {

  void onFindPlayer(dynamic data) {
    print("==========> ON FIND PLAYER: $data");
  }

  void onPlayQuestion(dynamic data) {
    print("==========> ON PLAY QUESTION: $data");
  }

  void onPlayReady(dynamic data) {
    print("==========> ON PLAY READY: $data");
  }

  void onPlayShowQuestion(dynamic data) {
    print("==========> ON PLAY SHOW QUESTION: $data");
  }

  void onPlayAnswer(dynamic data) {
    print("==========> ON PLAY ANSWER: $data");
  }

  void onEmotion(dynamic data) {
    print("==========> ON EMOTION: $data");
  }

  void onPlayEndGame(dynamic data) {
    print("==========> ON PLAY END GAME: $data");
  }

  void onOutRoom(dynamic data) {
    print("==========> ON ON OUT ROOM: $data");
  }

}