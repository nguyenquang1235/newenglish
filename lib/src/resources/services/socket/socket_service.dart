import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/resources/resources.dart';

class SocketService {
  static const String socket_choose_picture_listener =
      "/socket_choose_picture_listener";
  static const String socket_find_picture_listener =
      "/socket_find_picture_listener";
  static const String socket_combine =
      "/socket_combine";

  static SocketService _socketService;
  SocketIO _socketIO;
  dynamic _listener;
  SocketIOManager _manager;

  static Future<SocketService> instance() async {
    if (_socketService == null) {
      _socketService = SocketService();
      await _socketService.initSocket();
    }
    return _socketService;
  }

  initSocket() async {
    // String accessToken = await AppShared.getAccessToken();
    String uri = "${AppEndPoint.BASE_SOCKET_URL}:${AppEndPoint.PORT}";
    _manager = SocketIOManager();
    _socketIO = await _manager.createInstance(SocketOptions(
      uri,
      query: {"accessToken": AppEndPoint.TOKEN},
      enableLogging: true,
    ));

    setListener(SocketListener());

    _socketIO.onConnect(_listener.onSocketConnect);
    _socketIO.onDisconnect(_listener.onSocketDisconnect);

    _socketIO.connect();
  }

  int get id => _socketIO.id;

  Future<bool> get isConnected => _socketIO.isConnected();

  SocketListener get listener => _listener;

  setListener(SocketListener value) {
    _listener = value ?? SocketListener();
  }

  emitSocketDisconnect(Map<String, dynamic> params) {
    print("==========> EMIT DISCONNECT");
    _socketIO.emit("disconnect", [params]);
  }

  emitSocketConnect() async {
    await _socketIO.emit("connect", []);
    print("==========> EMIT CONNECT");
  }

  ///
  /// Game thách đấu chọn nhanh
  ///
  emitFindPlayer(int topicId) {
    Map<String, dynamic> params = {"topicid": topicId};
    print("==========> EMIT FIND PLAYER");
    _socketIO.emit("onFindPlayer", [params]);
  }

  emitPlayReady() {
    print("==========> EMIT PLAYER READY");
    _socketIO.emit("onPlay_Ready", []);
  }

  emitPlayAnswer({int index, int answer}) {
    Map<String, dynamic> params = {
      "questionIndex": index,
      "answer": answer,
    };
    print("==========> EMIT PLAYER ANSWER");
    _socketIO.emit("onPlay_Answer", [params]);
  }

  emitEmotion() {
    print("==========> EMIT EMOTION");
    _socketIO.emit("onEmotion", []);
  }

  emitOutRoom() {
    print("==========> OUT ROOM");
    _socketIO.emit("onOutRoom", []);
  }

  ///
  ///game thách đấu chọn ảnh
  ///

  emitSameFindPlayer(int topicId) {
    Map<String, dynamic> params = {"topicid": topicId};
    print("==========> EMIT FIND PLAYER");
    _socketIO.emit("onSameFindPlayer", [params]);
  }

  emitSamePlayReady() {
    print("==========> EMIT PLAYER READY");
    _socketIO.emit("onSamePlay_Ready", []);
  }

  emitSamePlayAnswer({int index1, int index2}) {
    Map<String, dynamic> params = {
      "index1": index1,
      "index2": index2,
    };
    print("==========> EMIT PLAYER ANSWER");
    _socketIO.emit("onSamePlay_Answer", [params]);
  }

  emitSameEmotion() {
    print("==========> EMIT EMOTION");
    _socketIO.emit("onSameOutRoom", []);
  }

  emitSameOutRoom() {
    print("==========> OUT ROOM");
    _socketIO.emit("onSameOutRoom", []);
  }

  ///
  ///game thách đấu tổng hợp
  ///

  emitCombineFindPlayer(int topicId) {
    Map<String, dynamic> params = {"topicid": topicId};
    print("==========> EMIT FIND PLAYER");
    _socketIO.emit("onCombineFindPlayer", [params]);
  }

  emitCombinePlayReady() {
    print("==========> EMIT PLAYER READY");
    _socketIO.emit("onCombinePlay_Ready", []);
  }

  emitCombinePlayAnswer({dynamic params}) {
    print("==========> EMIT PLAYER ANSWER");
    _socketIO.emit("onCombinePlay_Answer", [params]);
  }

  emitCombineEmotion() {
    print("==========> EMIT EMOTION");
    _socketIO.emit("onCombineOutRoom", []);
  }

  emitCombineOutRoom() {
    print("==========> OUT ROOM");
    _socketIO.emit("onCombineOutRoom", []);
  }

  void destroy() {
    if (_socketIO != null) _manager.clearInstance(_socketIO);
    // _socketService = null;
    _listener = null;
  }

  listenEvent(String nameListener) async {
    switch (nameListener) {
      case socket_choose_picture_listener:
        _socketIO.on("onFindPlayer", _listener.onFindPlayer);
        _socketIO.on("onPlay_Question", _listener.onPlayQuestion);
        _socketIO.on("onPlay_ShowQuestion", _listener.onPlayShowQuestion);
        _socketIO.on("onPlay_Answer", _listener.onPlayAnswer);
        _socketIO.on("onEmotion", _listener.onEmotion);
        _socketIO.on("onPlay_EndGame", _listener.onPlayEndGame);
        _socketIO.on("onOutRoom", _listener.onOutRoom);
        break;

      case socket_find_picture_listener:
        _socketIO.on("onSameFindPlayer", _listener.onSameFindPlayer);
        _socketIO.on("onSamePlay_Question", _listener.onSamePlayQuestion);
        _socketIO.on(
            "onSamePlay_ShowQuestion", _listener.onSamePlayShowQuestion);
        _socketIO.on("onSamePlay_Answer", _listener.onSamePlayAnswer);
        _socketIO.on("onSameEmotion", _listener.onSameEmotion);
        _socketIO.on("onSamePlay_EndGame", _listener.onSamePlayEndGame);
        _socketIO.on("onSameOutRoom", _listener.onSameOutRoom);
        break;

      case socket_combine:
        _socketIO.on("onCombineFindPlayer", _listener.onCombineFindPlayer);
        _socketIO.on("onCombinePlay_Question", _listener.onCombinePlayQuestion);
        _socketIO.on(
            "onCombinePlay_ShowQuestion", _listener.onCombinePlayShowQuestion);
        _socketIO.on("onCombinePlay_Answer", _listener.onCombinePlayAnswer);
        _socketIO.on("onCombineEmotion", _listener.onCombineEmotion);
        _socketIO.on("onCombinePlay_EndGame", _listener.onCombinePlayEndGame);
        _socketIO.on("onCombineOutRoom", _listener.onCombineOutRoom);
        break;
    }
  }

  offEvent(String nameListener) async {
    switch (nameListener) {
      case socket_choose_picture_listener:
        _socketIO.off("onFindPlayer", _listener.onFindPlayer);
        _socketIO.off("onPlay_Question", _listener.onPlayQuestion);
        _socketIO.off("onPlay_ShowQuestion", _listener.onPlayShowQuestion);
        _socketIO.off("onPlay_Answer", _listener.onPlayAnswer);
        _socketIO.off("onEmotion", _listener.onEmotion);
        _socketIO.off("onPlay_EndGame", _listener.onPlayEndGame);
        _socketIO.off("onOutRoom", _listener.onOutRoom);
        break;

      case socket_find_picture_listener:
        _socketIO.off("onSameFindPlayer", _listener.onSameFindPlayer);
        _socketIO.off("onSamePlay_Question", _listener.onSamePlayQuestion);
        _socketIO.off(
            "onSamePlay_ShowQuestion", _listener.onSamePlayShowQuestion);
        _socketIO.off("onSamePlay_Answer", _listener.onSamePlayAnswer);
        _socketIO.off("onSameEmotion", _listener.onSameEmotion);
        _socketIO.off("onSamePlay_EndGame", _listener.onSamePlayEndGame);
        _socketIO.off("onSameOutRoom", _listener.onSameOutRoom);
        break;

      case socket_combine:
        _socketIO.off("onCombineFindPlayer", _listener.onCombineFindPlayer);
        _socketIO.off("onCombinePlay_Question", _listener.onCombinePlayQuestion);
        _socketIO.off(
            "onCombinePlay_ShowQuestion", _listener.onCombinePlayShowQuestion);
        _socketIO.off("onCombinePlay_Answer", _listener.onCombinePlayAnswer);
        _socketIO.off("onCombineEmotion", _listener.onCombineEmotion);
        _socketIO.off("onCombinePlay_EndGame", _listener.onCombinePlayEndGame);
        _socketIO.off("onCombineOutRoom", _listener.onCombineOutRoom);
        break;
    }
  }
}
